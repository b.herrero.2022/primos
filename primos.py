n = int(input("Dame un numero entero no negativo"))
if n > 0:
    for i in range (2,n+1):
        creciente = 2
        esprimo = True
        while esprimo and creciente < i:
            if i % creciente == 0:
                esprimo = False
            else:
                creciente += 1
        if esprimo:
            print(i, "es numero primo igual o menor,", end='')
else:
    print("El numero no es valido")



